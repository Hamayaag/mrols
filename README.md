
# MRols

<!-- badges: start -->
<!-- badges: end -->

The goal of MRols is to stimate a linear models using parallel

## Installation

You can install the released version of MRols from [gitlab](git@gitlab.com:Hamayaag/mrols.git) with:

``` 
devtools:install_git("git@gitlab.com:Hamayaag/mrols.git")


## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(MRols)
## basic example code
```
set.seed(001)
X = data.frame(x1 = rnorm(100),
               x2 = rnorm(100))
y = as.vector(rnorm(100))
