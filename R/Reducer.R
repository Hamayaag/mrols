
Reducer <- function(Qi, Qip, yi){
  if(!is.matrix(Qi) | !is.matrix(Qip) | !is.matrix(yi) )
    stop('Qi, Qip  and yi should be a matrix')

  return( t(Qi %*%  Qip) %*% yi )
}

# Example

Xi = res[[1]][[2]]
Qi = Mapper(Xi)$Q
Reducer(Qi, t(Qi), as.matrix(res[[2]][[2]]))
